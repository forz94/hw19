#include <iostream>

using namespace std;

class Animal
{
public:
	virtual void	Voice()
	{
		cout << "It says: ";
	}
};

class	Dog : public Animal
{
public:
	void	Voice() override
	{
		cout << "\nWoof!";
	}
};

class	Cat : public Animal
{
public:
	void	Voice() override
	{
		cout << "\nMeow ^._.^";
	}
};

class	Cow : public Animal
{
public:
	void	Voice() override
	{
		cout << "\nMOO!";
	}
};

int		main()
{
	Animal*	a[4];
	a[0] = new Animal();
	a[1] = new Dog();
	a[2] = new Cat();
	a[3] = new Cow();
	for (Animal* i : a)
		i->Voice();
}
	